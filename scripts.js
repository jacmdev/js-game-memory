(function () {
  const ICONS = [
    'archway',
    'bus-alt',
    'car-side',
    'caravan',
    'cocktail',
    'compass',
    'glass-cheers',
    'globe-americas',
    'hotel',
    'ice-cream',
    'map-marked-alt',
    'plane',
    'road',
    'suitcase-rolling',
    'sun',
    'swimmer',
    'swimming-pool',
    'smile-beam',
    'umbrella-beach',
  ];
  const ICONS_ON_BOARD_COUNT = 12;
  const MAX_STORAGE_SCORES = 5;
  const SCORE_STORAGE_KEY = 'memoryScore';

  const $ = document.querySelector.bind(document);
  const $bestScores = $('#best-scores');
  const $board = $('#board');
  const $gameFinished = $('#game-finished');
  const $gameFinishedSfx = $('#game-finished-sfx');
  const $menuBtn = $('#menu-btn');
  const $menuBtnIcon = $('#menu-btn-icon');
  const $menuContent = $('#menu-content');
  const $music = $('#music');
  const $musicBtn = $('#music-btn');
  const $musicIcon = $('#music-btn-icon');
  const $newGameBtn = $('#new-game-btn');
  const $score = $('#score');
  const $time = $('#time');
  const $timeContainer = $('#time-container');
  const pad = (n) => String(n).padStart(2, '0');
  let activeIcons;
  let gameIcons;
  let solvedLength;
  let startTime;
  let timeInterval;
  let timeOnPause;

  init();

  function init() {
    createNewGame();
    addEventListeners();
  }

  function addEventListeners() {
    $board.addEventListener('click', onBoardClick);
    $menuBtn.addEventListener('click', onMenuContentToggle);
    $music.addEventListener('ended', () => $music.play());
    $musicBtn.addEventListener('click', onMusicToggle);
    $newGameBtn.addEventListener('click', onNewGame);
  }

  function checkMatchingPair() {
    const activeItem1 = $board.querySelector(`[data-index="${activeIcons[0]}"]`);
    const activeItem2 = $board.querySelector(`[data-index="${activeIcons[1]}"]`);

    if (gameIcons[activeIcons[0]] === gameIcons[activeIcons[1]]) {
      solvedLength += 1;

      if (solvedLength === ICONS_ON_BOARD_COUNT) {
        finishGame();
      }
    }

    window.setTimeout(() => {
      if (gameIcons[activeIcons[0]] === gameIcons[activeIcons[1]]) {
        activeItem1.classList.add('m--solved');
        activeItem2.classList.add('m--solved');
      }

      activeItem1.classList.remove('m--active');
      activeItem2.classList.remove('m--active');
      activeIcons = [];
    }, 1000);
  }

  function createBoard() {
    $board.innerHTML = '';
    gameIcons.forEach((icon, index) => {
      $board.insertAdjacentHTML(
        'beforeend',
        `
            <button class="board-item" data-index="${index}">
                <i class="fa fa-${icon}"></i>
            </button>
        `
      );
    });
  }

  function createNewGame() {
    reset();
    createBoard();
    $timeContainer.classList.remove('hidden');
  }

  function finishGame() {
    const time = Date.now() - startTime;
    $score.textContent = formatTime(time);
    $timeContainer.classList.add('hidden');
    window.clearInterval(timeInterval);
    $board.classList.add('hidden');
    $gameFinished.classList.remove('hidden');
    saveScore(time);

    if (isMusicOn()) {
      $gameFinishedSfx.play();
    }
  }

  function formatDate(date) {
    const d = new Date(date);
    return `${pad(d.getDate())}.${pad(d.getMonth() + 1)}.${d.getFullYear()}, 
    ${pad(d.getHours())}:${pad(d.getMinutes())}`;
  }

  function formatTime(time) {
    const minutes = pad(Math.floor(time / 60000));
    const seconds = pad(Math.floor(time / 1000) - minutes * 60);
    return `${minutes}:${seconds}`;
  }

  function getIcons() {
    const arr = [...ICONS];
    const newArr = [];

    for (let i = 0; i < ICONS_ON_BOARD_COUNT; i++) {
      const randomIcon = arr.splice(rand(0, arr.length - 1), 1);
      newArr.push(randomIcon, randomIcon);
    }

    return shuffleArray(newArr);
  }

  function isMusicOn() {
    return $musicIcon.classList.contains('fa-volume-up');
  }

  function onBoardClick(event) {
    const { target } = event;

    if (!target.classList.contains('board-item') || target.classList.contains('m--solved')) {
      return;
    }

    const { index } = target.dataset;

    if (activeIcons.length >= 2 || activeIcons.includes(index)) {
      return;
    }

    activeIcons.push(index);
    target.classList.add('m--active');

    if (activeIcons.length === 2) {
      checkMatchingPair();
    }
  }

  function onMenuContentToggle() {
    if ($menuContent.classList.contains('hidden')) {
      $menuBtnIcon.className = 'fa fa-times';
      $gameFinished.classList.add('hidden');
      window.clearInterval(timeInterval);
      timeOnPause = Date.now() - startTime;
      renderScores();
    } else {
      $menuBtnIcon.className = 'fa fa-bars';
      startTime = Date.now() - timeOnPause;
      updateTime();
      timeInterval = window.setInterval(updateTime, 1000);
    }

    $menuContent.classList.toggle('hidden');
    $board.classList.toggle('hidden');
  }

  function onMusicToggle() {
    if (isMusicOn()) {
      $music.pause();
      $musicIcon.className = 'fa fa-volume-mute';
    } else {
      $music.play();
      $musicIcon.className = 'fa fa-volume-up';
    }
  }

  function onNewGame() {
    $menuBtnIcon.className = 'fa fa-bars';
    $menuContent.classList.add('hidden');
    createNewGame();
    $board.classList.remove('hidden');
  }

  function rand(from, to) {
    return Math.floor(Math.random() * (to - from + 1)) + from;
  }

  function renderScores() {
    const savedScores = JSON.parse(window.localStorage.getItem(SCORE_STORAGE_KEY)) || [];
    $bestScores.innerHTML = '';

    if (savedScores.length === 0) {
      $bestScores.textContent = '-';
      return;
    }

    savedScores.forEach((item) => {
      $bestScores.insertAdjacentHTML('beforeend', `<li>${formatTime(item.score)} (${formatDate(item.date)})</li>`);
    });
  }

  function reset() {
    gameIcons = getIcons();
    activeIcons = [];
    solvedLength = 0;
    startTime = Date.now();
    timeInterval = window.setInterval(updateTime, 1000);
    $time.textContent = formatTime(0);
  }

  function saveScore(time) {
    const savedScores = JSON.parse(window.localStorage.getItem(SCORE_STORAGE_KEY)) || [];
    savedScores.push({ score: time, date: Date.now() });
    savedScores.sort((a, b) => a.score - b.score);
    window.localStorage.setItem(SCORE_STORAGE_KEY, JSON.stringify(savedScores.slice(0, MAX_STORAGE_SCORES)));
  }

  function shuffleArray(arr) {
    for (let i = arr.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [arr[i], arr[j]] = [arr[j], arr[i]];
    }

    return arr;
  }

  function updateTime() {
    $time.textContent = formatTime(Date.now() - startTime);
  }
})();
