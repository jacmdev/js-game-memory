# About

Simple game created with vanilla JS and CSS.

- Game logic
- Animations
- Time measure
- Best scores

# Game description

Find all matching pairs.

# Demo

https://jacmdev.com/js-game-memory/
